

default: push


push:
	git add .
	git commit -m "Content update"
	git push
	@echo "Site submitted for processing and publication."
	@echo "Status and result information can be checked at https://gitlab.com/dris_porto/site/pipelines"
	@echo "On success, new version will be available online in few minutes."
	@echo "Live version will be available at: https://dris_porto.gitlab.io/site/"
