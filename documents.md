---
layout: page
title: Documents
permalink: /docs/
---

* [Manual do Supervisor](supervisor.pdf)
* [Developer Guide](http://dris_porto.gitlab.io/dris/md_DevGuide.html)
* [Software Reference Manual](http://dris_porto.gitlab.io/dris/)


