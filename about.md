---
layout: page
title: About DriS Website
permalink: /about/
mainmenu: false
---

__Dris Website__ is built by [Jekyll](https://jekyllrb.com/) over the [GitLab-CI pipeline](https://docs.gitlab.com/ee/ci/pipelines.html) infrastructure and hosted by [GitLab Pages](https://pages.gitlab.io).

Source files requried to buid the __DriS Website__ are maintained in a git repository at [https://gitlab.com/dris_porto/dris_porto.gitlab.io](https://gitlab.com/dris_porto/dris_porto.gitlab.io).

Live __DriS Website__ can be found at [https://dris_porto.gitlab.io](https://dris_porto.gitlab.io).

(c) 2017, Dris Dev Team

