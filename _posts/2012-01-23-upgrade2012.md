---
layout: post
title:  "Upgrade 2012"
date:   2012-01-23 15:32:14 -0300
categories: update

items:
- title: Plataforma computacional
  desc: Plataforma computacional (hardware) de simulação com máquina potenciada por uma motherboard ASUS P8H67 com socket LGA1155 e um processador Intel Core i5 de 4 núcleos a 3.2 GHz.
  img: corei5.png
- title: Subsistema gráfico
  desc: Subsistema gráfico de síntese de imagem baseado numa placa gráfica ASUS com interface PCIe processador Nvidia GeForge GTX460 com 1GB GDDR5 e 2 saídas DVI.
  img: nvidia.png
- title: Barebone
  desc: Barebone NOX de suporte aos componentes de hardware composto por caixa ventilada por ventoinha de 14” e fonte de alimentação modular NOX de 800W.
  img: nox.png
- title: Gráficos 3D
  desc: Infraestrutura de gráficos 3D de código aberto, OpenSceneGraph v3.01.
  img: osg.png


---
<style type="text/css">
    .up_item { border-top: 2px solid #404040;  }
    .desc, .news-icon {display: inline-block; *display: inline; zoom: 1; vertical-align: top; font-size: 12px;}
    .desc { width: 80%; }
    .news-icon { width: 17%; margin-top: 2em; }
</style>

No primeiro semestre de 2012 o simulador de condução DriS vai sofrer uma atualização que inclui as seguintes alterações:

  <ul class="post-list">
    {% for item in page.items %}
      <li>

	<p>
		<div class="up_item"> 
			<div class="desc"> <h4> {{ item.title }} </h4> {{ item.desc }} </div>
			<div class="news-icon"> 
				<img src="/site/images/{{ item.img }}" alt="{{ item.title }}" height='80' >
			</div>
		</div>
        </p>
      </li>
    {% endfor %}
  </ul>


